const { Client } = require("./class-client");
const { Game } = require("./class-game");


exports.Server = class Server {

    constructor()
    {
        this.port = 8080;
        this.serverName = "Pong Server";
        this.clients = [];
        this.maxConnectedUsers = 8;

        //player list
        this.player1 = null;
        this.player2 = null;
        this.player3 = null;
        this.player4 = null;

        this.timeUntilNextBroadcast = 5;


        this.sock = require("dgram").createSocket("udp4");
        
        //setup listeners
        this.sock.on("error", (e) => this.onError(e));
        this.sock.on("listening", () => this.onStartListen());
        this.sock.on("message", (msg, rinfo) => this.onPacket(msg, rinfo));


        //create game
        this.game = new Game(this);

        this.sock.bind(this.port);
    }
    onError(e)
    {
        console.log("Error: " + e);
    }
    onStartListen()
    {
        console.log("Server is listening on port " + this.port);
    }
    onPacket(msg, rinfo)
    {
        if (msg.length < 4) return;
        
        const packetID = msg.slice(0, 4).toString();

        const c = this.lookupClient(rinfo);
        if (c)
        {
            c.onPacket(msg, this.game);
        }
        else
        {
            if (packetID == "JOIN")
            {
                console.log("Someone tried to join from " + rinfo.address);
                this.makeClient(rinfo);
            }
        }

        //lookup and create clients

    }

    disconnectClient(client)
    {
        if (client.pawn)
        {
            //TODO: remove

        }

        const key = this.getKeyFromRinfo(client.rinfo);
        delete this.clients[key];

        //TODO: show clients list
        this.showClientsList();
    }

    lookupClient(rinfo)
    {
        const key = this.getKeyFromRinfo(rinfo);
        return this.clients[key];
    }

    makeClient(rinfo)
    {
        const key = this.getKeyFromRinfo(rinfo);
        const client = new Client(rinfo);
        

        this.clients[key] = client;
        /*
            {
                "127.0.0.1:8080" : { rinfo: {}, input: {} etc etc},
                "127.0.0.1:8080" : { rinfo: {}, input: {} etc etc}
            }
        */
        
        const packet = this.game.makeREPL(false);

        this.sendPacketToClient(packet, client);

        client.spawnPawn(this.game);
        this.showClientsList();
        const packet2 = Buffer.alloc(5);
        packet2.write("PAWN", 0);
        packet2.writeUInt8(client.pawn.networkID, 4);
        this.sendPacketToClient(packet2, client);

        //make client a player if a slot is open
        if (!this.player1) player1 = client;
        if (!this.player2) player2 = client;
        if (!this.player3) player3 = client;
        if (!this.player4) player4 = client;
        //make client a spectator if all slots are filled

        //return client; //not really needed

    }

    getPlayer(num = 0)
    {
        num = parseInt(num);
        let i = 0;
        for (var key in this.clients)
        {
            if (num == i) return this.clients[key];

            i++;
        }
    }

    showClientsList()
    {
        console.log("======= " + Object.keys(this.clients).length + " clients connected =======");
        for (var key in this.clients)
        {
            console.log(key);
        }
    }

    getKeyFromRinfo(rinfo)
    {
        return rinfo.address + ":" + rinfo.port;
    }

    sendPacketToAll(packet)
    {
        for (var key in this.clients)
        {
            this.sendPacketToClient(packet, this.clients[key]);
        }
    }

    sendPacketToClient(packet, client)
    {
        console.log(client.rinfo);
        this.sock.send(packet, 0, packet.length, 8081, client.rinfo.address, () => {});
    }

    broadcastPacket(packet)
    {
        const clientListenPort = 8081;

        this.sock.send(packet, 0, packet.length, clientListenPort, undefined);
    }

    broadcastServerHost()
    {
        const nameLength = this.serverName.length;
        const packet = Buffer.alloc(7 + nameLength);

        packet.write("HOST", 0);
        packet.writeUInt16BE(this.port, 4);
        packet.writeUInt8(nameLength, 6);
        packet.write(this.serverName, 7);

        this.broadcastPacket(packet);
    }

    update(game) {
        //check clients for disconnects, etc
        for (let key in this.clients)
        {
            this.clients[key].update(game);
        }

        this.timeUntilNextBroadcast -= game.dt;
        if (this.timeUntilNextBroadcast <= 0)
        {
            this.timeUntilNextBroadcast = 1.5;

            this.broadcastServerHost();
        }

    }
}