const Game = require("./class-game.js").Game;
const Pawn = require("./class-pawn.js").Pawn;
exports.Client = class Client {

    static TIMEOUT = 8;

    constructor(rinfo)
    {
        this.rinfo = rinfo;

        this.input = {
            axisH: 0,
            axisV: 0
        }

        this.isReady = false;

        this.pawn = null;

        this.timeOfLastPacket = Game.Singleton.time; //measured in seconds
    }

    spawnPawn(game)
    {
        if (this.pawn) return;

        this.pawn = new Pawn();
        game.spawnObject(this.pawn); //spawns pawn
    }

    update()
    {
        const game = Game.Singleton;

        if (game.time > this.timeOfLastPacket + Client.TIMEOUT)
        {
            //disconnect
            game.server.disconnectClient(this);
        }
    }

    onPacket(packet, game)
    {
        if (packet.length < 4) return;

        const packetID = packet.slice(0, 4).toString();

        this.timeOfLastPacket = game.time;

        switch (packetID)
        {
            case "INPT":
                if (packet.length < 5) return;

                this.input.axisV = packet.ReadInt8(4);

                if (this.pawn) this.pawn.input = this.input;

                break;
            case "REDY":
                this.isReady = true;
                break;
            default:
                console.log("ERROR: Packet type not recognized");
        }
    }
}