const { NetworkObject } = require("./class-networkobject");


exports.Pawn = class Pawn extends NetworkObject
{

    constructor()
    {
        super();

        this.classID = "PAWN";

        this.velocity = {x:0, y:0, z:0};

        this.input = {};
    }

    accelerate(vel, acc, dt)
    {
        if (acc)
        {
            vel += acc * dt;
        }
        else
        {
            //not pressing up or down
            //slow down

            if (vel > 0) //moving up
            {
                acc = -1;
                vel += acc * dt;
                if (vel < 0) vel = 0;
            }

            if (vel < 0) //moving down
            {
                acc = 1;
                vel += acc * dt;
                if (vel > 0) vel = 0;
            }
        }

        return vel ? vel : 0;
    }

    update(game)
    {
        let moveY = this.input.axisV;

        this.velocity.y = this.accelerate(this.velocity.y, moveY, game.dt);

        this.position.y += this.velocity.x * game.dt;
    }

    serialize()
    {
        let b = super.serialize();

        return b;
    }

    deserialize()
    {
        //TODO...
    }
}