﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using TMPro;
using System.Net.Sockets;
using UnityEngine;
using UnityEngine.UI;
using System.Threading;

public class ClientUDP : MonoBehaviour
{

    private static ClientUDP _singleton;
    public static ClientUDP singleton
    {
        get { return _singleton; }
        private set { _singleton = value; }
    }

    static UdpClient sockSending;
    static UdpClient sockReceive = new UdpClient(8081);

    public List<RemoteServer> availableGameServers = new List<RemoteServer>();
    public Transform ball;

    public GameObject PanelConnection;
    public GameObject PanelLobby;
    public GameObject PanelGameplay;
    public GameObject GameplayElements;

    public TMP_InputField inputHost;
    public TMP_InputField inputPort;
    public TMP_InputField inputUsername;

    public Button directConnect;
    public Button disconnect;
    public Button ready;
    public Text serverName;

    private RemoteServer directConnectServer;

    public enum Panel
    {
        Connection,
        Lobby,
        Gameplay
    }

    //the last frame # we got from the server;
    public uint lastBallUpdate = 0;

    public void Init(RemoteServer directConnectServer)
    {
        this.directConnectServer = directConnectServer;

        serverName.text = directConnectServer.serverName;

        directConnect.onClick.AddListener(OnButtonDirectConnect);
        disconnect.onClick.AddListener(OnButtonDisconnect);
        ready.onClick.AddListener(OnButtonReady);
    }

    // Start is called before the first frame update
    void Start()
    {

        if (singleton != null)
        {
            Destroy(gameObject);
        }
        else
        {
            singleton = this;
            //set up receive loop
            ListenForPackets();

        }


        //go to starting state
        GoToPanel(Panel.Connection);

    }

    public void GoToPanel(Panel panel)
    {
        if (panel == Panel.Connection)
        {
            PanelGameplay.SetActive(false);
            PanelConnection.SetActive(true);
            PanelLobby.SetActive(false);
            GameplayElements.SetActive(false);
        }
        if (panel == Panel.Lobby)
        {
            PanelGameplay.SetActive(false);
            PanelConnection.SetActive(false);
            PanelLobby.SetActive(true);
            GameplayElements.SetActive(false);

            /*
            foreach (ServerRowGUI row in rows)
            {
                if (row != null)
                Destroy (row.gameObject);
            }
            //delete old prefabs
            rows.Clear();
            */
        }

        if (panel == Panel.Gameplay)
        {
            PanelGameplay.SetActive(true);
            PanelConnection.SetActive(false);
            PanelLobby.SetActive(false);
            GameplayElements.SetActive(true);

            /*
            foreach (ServerRowGUI row in rows)
            {
                if (row != null)
                Destroy (row.gameObject);
            }
            //delete old prefabs
            rows.Clear();
            */
        }
    }

    public void ConnectToServer(string host, ushort port)
    {
        print($"attempt to connect to {host}:{port}");

        //TODO: don't do anything if we're already connected

        IPEndPoint ep = new IPEndPoint(IPAddress.Parse(host), port);
        sockSending = new UdpClient(ep.AddressFamily);
        sockSending.Connect(ep);

        SendPacket(Buffer.From("JOIN"));
        GoToPanel(Panel.Lobby);
    }

      
    public void OnButtonDirectConnect()
    {
        string host = inputHost.text;
        UInt16.TryParse(inputPort.text, out ushort port);

        ConnectToServer(directConnectServer.endPoint.Address.ToString(), (ushort)directConnectServer.endPoint.Port);
    }
    
    public void OnButtonDisconnect()
    {
        //TODO: send a packet to server

        GoToPanel(Panel.Connection);
    }

    public void OnButtonReady()
    {
        string username = inputUsername.text;

        //TODO: send a packet to server
        SendPacket(Buffer.From("REDY"));

        //GoToPanel(Panel.Gameplay);
    }

    public void GameOver()
    {

        //5 second delay
        Thread.Sleep(5000);
        GoToPanel(Panel.Lobby);
    }

    private async void ListenForPackets()
    {
        while (true)
        {
            UdpReceiveResult result;
            try
            {
                result = await sockReceive.ReceiveAsync();

                //process the packet
                ProcessPacket(result);
            }
            catch
            {
                break;
            }
        }
    }

    private void ProcessPacket(UdpReceiveResult res)
    {

        Buffer packet = Buffer.From(res.Buffer);

        if (packet.Length < 4) return;

        string id = packet.ReadString(0, 4);
        

        switch (id)
        {
            // case "BALL":
            //     if (packet.Length < 16) return;

            //     uint frameNumber = packet.ReadUInt32BE(4);

            //     if (frameNumber < lastBallUpdate) return; //ignore this packet

            //     lastBallUpdate = frameNumber;
            //     print(lastBallUpdate);
            //     float x = packet.ReadSingleLE(8);
            //     float y = packet.ReadSingleLE(12);
            //     float z = packet.ReadSingleLE(16);

            //     ball.position = new Vector3(x, y, z);

            //     break;
            case "REPL":
                ProcessPacketREPL(packet);

                break;
            case "PAWN":
                if (packet.Length < 5) return;

                byte networkID = packet.ReadUInt8(4);
                NetworkObject obj = NetworkObject.GetObjectByNetworkID(networkID);
                if (obj)
                {
                    Pawn p = (Pawn)obj;
                    if (p != null) p.canPlayerControl = true;
                }

                break;
            case "HOST":
                if (packet.Length < 7) return;

                ushort port = packet.ReadUInt16BE(4);
                int nameLength = packet.ReadUInt8(6);

                if (packet.Length < 7 + nameLength) return;

                string name = packet.ReadString(7, nameLength);

                //do something with this info
                AddToServerList(new RemoteServer(res.RemoteEndPoint, name));
                break;
        }
    }

    private void ProcessPacketREPL(Buffer packet)
    {
        if (packet.Length < 5) return; //do nothing

        int replType = packet.ReadUInt8(4);
        int offset = 5;
        if (replType < 1 || replType > 3) return; //do nothing

        while (offset <= packet.Length)
        {
            int networkID = 0;

            switch (replType)
            {
                case 1: //create
                    if (packet.Length < offset + 5) return;

                    networkID = packet.ReadUInt8(offset + 4);

                    string classID = packet.ReadString(offset, 4);

                    if (NetworkObject.GetObjectByNetworkID(networkID) != null) return; //object exists

                    NetworkObject obj = ObjectRegistry.SpawnFrom(classID);
                    if (obj == null) return; //ERROR

                    offset += 4;
                    offset += obj.Deserialize(packet.Slice(offset));

                    NetworkObject.AddObject(obj);

                    break;
                case 2: //update

                    if (packet.Length < offset + 5) return;
                    networkID = packet.ReadUInt8(offset + 4);

                    NetworkObject obj2 = NetworkObject.GetObjectByNetworkID(networkID);
                    if (obj2 == null) return;

                    offset += 4;
                    offset += obj2.Deserialize(packet.Slice(offset));

                    break;
                case 3: //delete

                    if (packet.Length < offset + 1) return;

                    networkID = packet.ReadUInt8(offset);

                    NetworkObject obj3 = NetworkObject.GetObjectByNetworkID(networkID);
                    if (obj3 == null) return;

                    NetworkObject.RemoveObject(networkID);

                    Destroy(obj3.gameObject);
                    offset++;

                    break;
            }
        }
    }

    private void AddToServerList(RemoteServer server)
    {
        //check if it exists
        if (!availableGameServers.Contains(server))
        {
            availableGameServers.Add(server);
        }


    }
    public async void SendPacket(Buffer packet)
    {
        if (sockSending == null) return;
        if (!sockSending.Client.Connected) return;

        await sockSending.SendAsync(packet.Bytes, packet.Bytes.Length);
    }

    private void OnDestroy()
    {
        if (sockSending != null) sockSending.Close();
        if (sockReceive != null) sockReceive.Close();
    }
}
