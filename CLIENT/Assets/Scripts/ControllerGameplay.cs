using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public enum Player
{
    Spectator,
    Player1,
    Player2,
    Player3,
    Player4
}

public class ControllerGameplay : MonoBehaviour
{

    public Text scoreboard;

    public int team1Score = 0;
    public int team2Score = 0;

    void Start()
    {

    }

    public void UpdateFromServer()
    {

        //gameplay code







        //find out score

        scoreboard.text = team1Score + " - " + team2Score;

        if (team1Score >= 5)
        {
            scoreboard.text = "Team 1 wins the game. Returning to lobby.";
            ClientUDP.singleton.GameOver();
        }
        if (team2Score >= 5)
        {
            scoreboard.text = "Team 2 wins the game. Returning to lobby.";
            ClientUDP.singleton.GameOver();
        }
    }

}
