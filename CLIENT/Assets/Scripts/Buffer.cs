﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class Buffer
{

    public static Buffer Alloc(int size)
    {
        return new Buffer(size);
    }


    public static Buffer From(string txt)
    {
        Buffer b = new Buffer(txt.Length);
        b.WriteString(txt);
        return b;
    }

    public static Buffer From(byte[] items)
    {
        Buffer b = new Buffer(items.Length);
        b.WriteBytes(items);
        return b;
    }

    public int Length
    {
        get
        {
            return bytes.Length;
        }
    }

        private byte[] bytes;

    public byte[] Bytes
    {
        get
        {
            return bytes;
        }
    }

    public void Concat(byte[] newdata, int numOfBytes = -1)
    {

        if(numOfBytes < 0 || numOfBytes > newdata.Length) numOfBytes = newdata.Length;

        byte[] newbytes = new byte[bytes.Length + numOfBytes];

        for (int i = 0; i < newbytes.Length; i++)
        {
            if (i < bytes.Length)
            {
                newbytes[i] = bytes[i];
            }
            else
            {
                newbytes[i] = newdata[i - bytes.Length];
            }
        }

        bytes = newbytes;

    }

    public void Concat(Buffer otherbuff)
    {
        Concat(otherbuff.bytes);
    }



    private Buffer(int size = 0)
    {
        if (size < 0) size = 0;
        bytes = new byte[size];
    }

    public void Clear()
    {
        bytes = new byte[0];
    }

    public void Consume(int numOfBytes)
    {
        int newLength = bytes.Length - numOfBytes;
        if (newLength >= bytes.Length) return;

        if (newLength <= 0)
        {
            bytes = new byte[0];
            return;
        }

        // 1 2 3 4 5 6 7
        //consume 3 bytes
        // 4 5 6 7

        byte[] newBytes = new byte[newLength];

        for (int i = 0; i < newBytes.Length; i++)
        {
            newBytes[i] = bytes[i + numOfBytes];
        }

        bytes = newBytes;
    }

    public Buffer Slice(int offset, int length = -1)
    {
        if (offset < 0) offset = 0;
        if (length < 0) length = bytes.Length - offset;

        if (offset + length > bytes.Length) return Buffer.Alloc(0);
        if (length <= 0) return Buffer.Alloc(0);

        byte[] newbytes = new byte[length];

        int j = 0;
        for (int i = offset; i < offset + length; i++)
        {
            newbytes[j++] = bytes[i];
        }

        return Buffer.From(newbytes);
    }

    public override string ToString()
    {
        //<Buffer 00 00 00 00>
        StringBuilder sb = new StringBuilder("<Buffer ");

        foreach (byte b in bytes)
        {
            sb.Append("  ");
            sb.Append(b.ToString("x2"));
        }

        sb.Append(">");

        return sb.ToString();
    }

    #region Read Integers

    //this is an alias
    public byte ReadByte(int offset = 0)
    {
        return ReadUInt8(offset);
    }

    public byte ReadUInt8(int offset = 0)
    {
        if (offset < 0 || offset >= bytes.Length)
        {
            Debug.LogError("Offset outside of byte array bounds.");
            return 0;
        }

        return bytes[offset];
    }
    
    public sbyte ReadInt8(int offset = 0)
    {
        return (sbyte)ReadByte(offset);
    }

    // [00000000, 00000000, 0000000, 00000000]

    public ushort ReadUInt16LE(int offset = 0)
    {
        byte a = ReadByte(offset);
        byte b = ReadByte(offset + 1);

        //bitwise operation
        return (ushort)((b << 8) | a);
    }

    public ushort ReadUInt16BE(int offset = 0)
    {
        byte a = ReadByte(offset);
        byte b = ReadByte(offset + 1);

        //bitwise operation
        return (ushort)((a << 8) | b);
    }

    public short ReadInt16LE(int offset = 0)
    {
        byte a = ReadByte(offset);
        byte b = ReadByte(offset + 1);

        //bitwise operation
        return (short)((b << 8) | a);
    }

    public short ReadInt16BE(int offset = 0)
    {
        byte a = ReadByte(offset);
        byte b = ReadByte(offset + 1);

        //bitwise operation
        return (short)((a << 8) | b);
    }

    public uint ReadUInt32LE(int offset = 0)
    {
        byte a = ReadByte(offset);
        byte b = ReadByte(offset + 1);
        byte c = ReadByte(offset + 2);
        byte d = ReadByte(offset + 3);

        //bitwise operation
        return (uint)((d << 24) | (c << 16) | (b << 8) | a);
    }

    public uint ReadUInt32BE(int offset = 0)
    {
        byte a = ReadByte(offset);
        byte b = ReadByte(offset + 1);
        byte c = ReadByte(offset + 2);
        byte d = ReadByte(offset + 3);

        //bitwise operation
        return (uint)((a << 24) | (b << 16) | (c << 8) | d);
    }

    public int ReadInt32LE(int offset = 0)
    {
        byte a = ReadByte(offset);
        byte b = ReadByte(offset + 1);
        byte c = ReadByte(offset + 2);
        byte d = ReadByte(offset + 3);

        //bitwise operation
        return (int)((d << 24) | (c << 16) | (b << 8) | a);
    }

    public int ReadInt32BE(int offset = 0)
    {
        byte a = ReadByte(offset);
        byte b = ReadByte(offset + 1);
        byte c = ReadByte(offset + 2);
        byte d = ReadByte(offset + 3);

        //bitwise operation
        return (int)((a << 24) | (b << 16) | (c << 8) | d);
    }
    #endregion

    #region Write Integers

    public void WriteByte(byte val, int offset = 0)
    {
        WriteUInt8(val, offset);
    }

    public void WriteUInt8(byte val, int offset = 0)
    {
        if(offset < 0 || offset >= bytes.Length) return;
        bytes[offset] = val;
    }

    public void WriteBytes(byte[] vals, int offset = 0)
    {
        for (int i = 0; i < vals.Length; i++)
        {
            WriteByte(vals[i], offset + i);
        }
    }

    //TODO: read signed 8-bit
    public void WriteInt8(sbyte val, int offset = 0)
    {
        WriteByte((byte)val, offset);
    }

        public void WriteUInt16LE(ushort val, int offset = 0)
    {
        WriteByte((byte) val, offset);
        WriteByte((byte)(val >> 8), offset + 1);
    }

    public void WriteUInt16BE(ushort val, int offset = 0)
    {
        WriteByte((byte)(val >> 8), offset);
        WriteByte((byte) val, offset + 1);
    }

    public void WriteInt16LE(short val, int offset = 0)
    {
        WriteByte((byte) val, offset);
        WriteByte((byte)(val >> 8), offset + 1);
    }

    public void WriteInt16BE(short val, int offset = 0)
    {
        WriteByte((byte)(val >> 8), offset);
        WriteByte((byte) val, offset + 1);
    }
    //TODO: read unsigned 32-bit LE
    //TODO: read unsigned 32-bit BE
    //TODO: read signed 32-bit LE
    //TODO: read signed 32-bit BE

    public void WriteUInt32LE(uint val, int offset = 0)
    {
        WriteByte((byte) val, offset);
        WriteByte((byte)(val >> 8), offset + 1);
        WriteByte((byte)(val >> 16), offset + 2);
        WriteByte((byte)(val >> 24), offset + 3);
    }

    public void WriteUInt32BE(uint val, int offset = 0)
    {
        WriteByte((byte) val, offset);
        WriteByte((byte)(val << 8), offset + 1);
        WriteByte((byte)(val << 16), offset + 2);
        WriteByte((byte)(val << 24), offset + 3);
    }

    public void WriteInt32LE(int val, int offset = 0)
    {
        WriteByte((byte) val, offset);
        WriteByte((byte)(val >> 8), offset + 1);
        WriteByte((byte)(val >> 16), offset + 2);
        WriteByte((byte)(val >> 24), offset + 3);
    }

    public void WriteInt32BE(int val, int offset = 0)
    {
        WriteByte((byte) val, offset);
        WriteByte((byte)(val << 8), offset + 1);
        WriteByte((byte)(val << 16), offset + 2);
        WriteByte((byte)(val << 24), offset + 3);
    }

    //TODO: read unsigned 8-bit
    //TODO: read signed 8-bit
    //TODO: read unsigned 16-bit BE
    //TODO: read signed 16-bit LE
    //TODO: read signed 16-bit BE
    //TODO: read unsigned 32-bit LE
    //TODO: read unsigned 32-bit BE
    //TODO: read signed 32-bit LE
    //TODO: read signed 32-bit BE
    //TODO: read unsigned 64-bit LE
    //TODO: read unsigned 64-bit BE
    //TODO: read signed 64-bit LE
    //TODO: read signed 64-bit BE
    #endregion

    #region Read Floats

    public float ReadSingleBE(int offset = 0)
    {
        return BitConverter.ToSingle(bytes, offset);
    }

    public float ReadSingleLE(int offset = 0)
    {
        byte[] temp = new byte[]
        {
            ReadByte(offset + 3),
            ReadByte(offset + 2),
            ReadByte(offset + 1),
            ReadByte(offset + 0)
        };

        return BitConverter.ToSingle(temp, 0);
    }


    public double ReadDoubleBE(int offset = 0)
    {
        return BitConverter.ToDouble(bytes, offset);
    }

    public double ReadDoubleLE(int offset = 0)
    {
        byte[] temp = new byte[]
        {
            ReadByte(offset + 7),
            ReadByte(offset + 6),
            ReadByte(offset + 5),
            ReadByte(offset + 4),
            ReadByte(offset + 3),
            ReadByte(offset + 2),
            ReadByte(offset + 1),
            ReadByte(offset + 0)
        };

        return BitConverter.ToDouble(temp, 0);
    }

    #endregion

    #region Write Floats

    public void WriteSingleBE(float val, int offset = 0)
    {
        byte[] parts = BitConverter.GetBytes(val);

        WriteBytes(parts, offset);
        //WriteByte(parts[0], offset + 0);
        //WriteByte(parts[1], offset + 1);
        //WriteByte(parts[2], offset + 2);
        //WriteByte(parts[3], offset + 3);

    }

    public void WriteSingleLE(float val, int offset = 0)
    {
        byte[] parts = BitConverter.GetBytes(val);

        WriteByte(parts[3], offset + 0);
        WriteByte(parts[2], offset + 1);
        WriteByte(parts[1], offset + 2);
        WriteByte(parts[0], offset + 3);

    }

    public void WriteDoubleBE(double val, int offset = 0)
    {
        byte[] parts = BitConverter.GetBytes(val);

        WriteBytes(parts, offset);
        //WriteByte(parts[0], offset + 0);
        //WriteByte(parts[1], offset + 1);
        //WriteByte(parts[2], offset + 2);
        //WriteByte(parts[3], offset + 3);

    }

    public void WriteDoubleLE(double val, int offset = 0)
    {
        byte[] parts = BitConverter.GetBytes(val);

        WriteByte(parts[7], offset + 0);
        WriteByte(parts[6], offset + 1);
        WriteByte(parts[5], offset + 2);
        WriteByte(parts[4], offset + 3);
        WriteByte(parts[3], offset + 4);
        WriteByte(parts[2], offset + 5);
        WriteByte(parts[1], offset + 6);
        WriteByte(parts[0], offset + 7);

    }

    #endregion

    #region Read Strings

    public string ReadString(int offset = 0, int length = 0)
    {
        if (length <= 0) length = bytes.Length;

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < length; i++)
        {
            if (i + offset >= bytes.Length) break;
            sb.Append((char)ReadByte(offset + i));
        }

        return sb.ToString();
    }

    #endregion

    #region Write Strings

    public void WriteString(string str, int offset = 0)
    {
        char[] chars = str.ToCharArray();
        for (int i = 0; i < chars.Length; i++)
        {
            if( offset + i >= bytes.Length) break;
            char c = chars[i];
            WriteByte((byte)c, offset + i);
        }
    }

    #endregion

    #region Read Bools

    public bool ReadBool(int offset = 0)
    {
        return (ReadByte(offset) > 0);
    }

    public bool[] ReadBitField(int offset = 0)
    {
        bool[] res = new bool[8];
        byte b = ReadByte(offset);

        res[0] = (b & 0b00000001) > 0;
        res[1] = (b & 0b00000010) > 0;
        res[2] = (b & 0b00000100) > 0;
        res[3] = (b & 0b00001000) > 0;
        res[4] = (b & 0b00010000) > 0;
        res[5] = (b & 0b00100000) > 0;
        res[6] = (b & 0b01000000) > 0;
        res[7] = (b & 0b10000000) > 0;

        return res;
    }

    #endregion

    #region Write Bools

    public void WriteBool(bool val, int offset = 0)
    {
        byte b = (byte)(val ? 1 : 0);
        WriteByte(b, offset);
    }

    public void WriteBitField(bool[] bits, int offset = 0)
    {
        if (bits.Length < 8) return; //plz no crash

        byte val = 0;

        //calculate the byte
        if(bits[0]) val |= (byte)(1 << 0);
        if(bits[0]) val |= (byte)(1 << 1);
        if(bits[0]) val |= (byte)(1 << 2);
        if(bits[0]) val |= (byte)(1 << 3);
        if(bits[0]) val |= (byte)(1 << 4);
        if(bits[0]) val |= (byte)(1 << 5);
        if(bits[0]) val |= (byte)(1 << 6);
        if(bits[0]) val |= (byte)(1 << 7);

        WriteByte(val, offset);
    }

    #endregion

}
