using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConnectGUI : MonoBehaviour
{

    public ServerRowGUI prefab;

    private List<ServerRowGUI> rows = new List<ServerRowGUI>();

    float timeUntilRefresh = 1;

    private void Update()
    {
        timeUntilRefresh -= Time.deltaTime;

        if (timeUntilRefresh <= 0)
        {
            timeUntilRefresh = 2;
            //update the server list
            UpdateServerList();
        }
    }

    public void UpdateServerList() 
    {
        foreach (ServerRowGUI row in rows)
        {
            if (row != null)
            Destroy (row.gameObject);
        }
        //delete old prefabs
        rows.Clear();

        //check the clientUDP singleton
        int i = 0;
        foreach (RemoteServer server in ClientUDP.singleton.availableGameServers)
        {
            //spawn a prefab
            ServerRowGUI row = Instantiate(prefab, transform);

            RectTransform rect = (RectTransform) row.transform;
            rect.localScale = Vector3.one;
            rect.sizeDelta = new Vector2(500, 50);
            rect.anchorMax = rect.anchorMin = new Vector2(0.5f, 0.5f);
            
            rect.anchoredPosition = new Vector2(-450, -i * 70);

            row.Init(server);
            rows.Add(row);
            i++;
        }
    }
}
