using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkObject : MonoBehaviour
{
    public int networkID;
    public static string classID = "NWOB";

    private static Dictionary<int, NetworkObject> currentObjects = new Dictionary<int, NetworkObject>();

    public static void AddObject(NetworkObject obj)
    {
        if (!currentObjects.ContainsKey(obj.networkID))
        {
            currentObjects.Add(obj.networkID, obj);
        }
    }

    public static void RemoveObject(int networkID)
    {
        if (currentObjects.ContainsKey(networkID))
        {
            currentObjects.Remove(networkID);
        }
    }

    public static void RemoveObject(NetworkObject obj)
    {
        RemoveObject(obj.networkID);
    }

    public static NetworkObject GetObjectByNetworkID(int networkID)
    {
        if (!currentObjects.ContainsKey(networkID)) return null;

        return currentObjects[networkID];
    }

    public virtual void Serialize()
    {
        //pointless??
    }

    public virtual int Deserialize(Buffer packet)
    {
        networkID = packet.ReadUInt8(0);

        float px = packet.ReadSingleLE(1);
        float py = packet.ReadSingleLE(5);
        float pz = packet.ReadSingleLE(9);

        float rx = packet.ReadSingleLE(13);
        float ry = packet.ReadSingleLE(17);
        float rz = packet.ReadSingleLE(21);

        float sx = packet.ReadSingleLE(25);
        float sy = packet.ReadSingleLE(29);
        float sz = packet.ReadSingleLE(33);

        transform.position = new Vector3(px, py, pz);
        transform.rotation = Quaternion.Euler(rx, ry, rz);
        transform.localScale = new Vector3(sx, sy, sz);

        return 37;
    }
}
