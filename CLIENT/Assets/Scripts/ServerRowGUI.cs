using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ServerRowGUI : MonoBehaviour
{
    public Text serverName;
    public Button button;

    private RemoteServer server;

    public void Init(RemoteServer server)
    {
        this.server = server;

        serverName.text = server.serverName;

        button.onClick.AddListener(OnConnectClicked);
    }

    public void OnConnectClicked()
    {
        //connect to server
        ClientUDP.singleton.ConnectToServer(server.endPoint.Address.ToString(), (ushort)server.endPoint.Port);
    }
}
