using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PacketBuilder
{

    static int previousInputH = 0;


    public static Buffer CurrentInput()
    {
        int h = (int) Input.GetAxisRaw("Horizontal");

        if (h == previousInputH) return null;

        previousInputH = h;

        Buffer b = Buffer.Alloc(5);
        b.WriteString("INPT", 0);
        b.WriteInt8((sbyte)h, 4);

        return b;
    }
}
